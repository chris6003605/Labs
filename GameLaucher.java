import java.util.Scanner;
public class GameLaucher{

	public static void main(String[] args){
	Scanner input = new Scanner(System.in);
	System.out.println("Hello User");
	System.out.println("Type 1 to play Wordle");
	System.out.println("Type 2 to play Hangman");
	int userNumber = input.nextInt();
		if(userNumber == 1 ||  userNumber == 2)
		{
			if (userNumber == 1){
				String generatedWord = Wordle.generateWord();
				Wordle.runGames(generatedWord);
			}
			else{
				//Get user input
				System.out.println("Enter a 4-letter word:");
				String word = input.next();
				
				//Convert to upper case
				word = word.toUpperCase();
			
				//Start hangman game
				Hangman.runGame(word);	
				}
			}
			else{
				System.out.println("WRITE 1 OR 2!");
			}
	}
}
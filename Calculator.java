import java.util.Random;

public class Calculator {

    public static int sumNumber(int num1, int num2) {


        return num1 + num2;
    }

    public static double squareRoot(int num1) {
        return Math.sqrt(num1);
    }

    public static double randomNumber() {
        return Math.random();
    }

    public static double dividedNumber(double num1, double num2) {
        if (num1 == 0 || num2 == 0) {
            System.out.println("Do not use the number 0!");
            return 0;
        }
		return num1/num2;

    }
}